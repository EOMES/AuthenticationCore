﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace AuthenticationCore
{
    public class XomesAuthenticationHandler : AuthenticationHandler<XomesAuthenticationOptions>
    {
        private readonly IOptionsMonitor<XomesAuthenticationOptions> options;
        private readonly ILoggerFactory logger;
        private readonly UrlEncoder encoder;
        private readonly ISystemClock clock;

        public XomesAuthenticationHandler(IOptionsMonitor<XomesAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
            this.options = options;
            this.logger = logger;
            this.encoder = encoder;
            this.clock = clock;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {

            return AuthenticateResult.NoResult();
        }
    }
}
