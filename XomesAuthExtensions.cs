﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticationCore
{
    public static class XomesAuthExtensions
    {
        public static AuthenticationBuilder AddXomesAuthenticaion(this AuthenticationBuilder authenticationBuilder, Action<XomesAuthenticationOptions> configureOptions)
        {
            return authenticationBuilder.AddScheme<XomesAuthenticationOptions, XomesAuthenticationHandler>("XomesScheme", "XomesAuth", configureOptions);
        }
    }
}
