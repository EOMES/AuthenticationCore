﻿using System;

namespace AuthenticationCore
{
    public class Authenticator : IAuthenticator
    {
        public Protocol Protocol { get; set; } = Protocol.Http;
        public string Host { get; set; }
        public ushort? Port { get; set; }

        public bool AuthenticateToken(string token, string role)
        {
            return true;
        }

        public void InvalidateCache(string token)
        {

        }
    }
}
