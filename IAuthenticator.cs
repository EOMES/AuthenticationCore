﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticationCore
{
    public interface IAuthenticator
    {
        Protocol Protocol { get; set; }
        string Host { get; set; }
        ushort? Port { get; set; }

        bool AuthenticateToken(string token, string role);
        void InvalidateCache(string token);

    }

    public enum Protocol
    {
        Http,
        Https,
    }
}
